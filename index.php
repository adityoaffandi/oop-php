<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OOP</title>
</head>
<body>
    <h1>OOP</h1>

    <?php

    // release 0
    echo "<h3>Release 0</h3>";
    require("animal.php");
    $sheep = new animal("shaun");

    echo $sheep->name . "<br>"; // "shaun"
    echo $sheep->legs . "<br>"; // 2
    // echo $sheep->cold_blooded . "<br>"; // false
    echo var_dump($sheep->cold_blooded) . "<br>";


    // release 1
    echo "<br><br><h3>Release 1</h3>";
    echo "<h4>Ape kera sakti</h4>";
    require("ape.php");
    $sungokong = new ape("kera sakti");
    $sungokong->yell(); // "Auooo"

    echo "<h4>Frog Buduk</h4>";
    require("frog.php");
    $kodok = new frog("buduk");
    $kodok->jump(); // "hop hop"

    ?>

</body>
</html>